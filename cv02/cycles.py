import random

def find_min_max(arr):
    max_a = arr[0]
    min_a = arr[0]

    for a in arr:
        if a > max_a:
            max_a = a
        elif a < min_a:
            min_a = a

    return min_a, max_a


## Testing cycles
if __name__ == "__main__":
    iters = 10

    for i in range(iters):
        if i == 5:
            break
        print(i, end="")
    print("")

    j = 0
    success = False
    while j < iters and not success:
        print(j, end=" ")
        j += 1
        if j == 5:
            success = True
    print(" ")

    random.seed(0)
    arr = [random.randint(0, 50) for _ in range(10)]
    print("arr", arr)
    min_a, max_a = find_min_max(arr)

    print("Array maximum =", max_a, sep="")
    print("Array minimum =", min_a)

    arr2 = [random.randint(0, 50) for _ in range(10)]
    print("arr2", arr2)
    res = find_min_max(arr2)
    print(res)
    res.append(-2)
    print(type(res))
    print(res)

    a, b, c = 1, 2, 4
    ## Default print: sep = " ", end = "\n"
    print("Vars", a, b, c, sep="--", end="/")
    print("control")