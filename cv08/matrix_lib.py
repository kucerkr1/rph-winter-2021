import random
## Originally named 2d_arr, this file contains
## the matrix functions specified in programming
## homework from labs.

def generate_matrix(rows, cols):
    matrix = [[random.randint(0, 1)
               for _ in range(cols)]
              for _ in range(rows)]
    return matrix

def print_matrix(matrix):
    for r in range(len(matrix)):
        for c in range(len(matrix[r])):
            print(matrix[r][c], end=" ")
        print("\n", end="")


def is_in_limits(v, v_min, v_max):
    return v >= v_min and v < v_max

def check_direction(data, r, c, dir):
    count = 0
    value = data[r][c]
    r2, c2 = r+dir[0], c+dir[1]
    while is_in_limits(r2, 0, len(data)) and \
          is_in_limits(c2, 0, len(data[r2])) and \
          data[r2][c2] == value:
        count += 1
        r2, c2 = r2+dir[0], c2+dir[1]
    return count

def line_size(matrix, r, c):
    directions = [(0, -1), (0, 1)]
    count = 1   ## the current position
    for dir in directions:
        count += check_direction(matrix, r, c, dir)
    return count

def line_column_size(matrix, r, c):
    directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
    count = 1   ## the current position

    for dir in directions:
        count += check_direction(matrix, r, c, dir)
    return count

def region_size(matrix, r, c):
     count = 1   ## the current position

    ## find directions in 8-neighborhood
    for r in (-1, 0, 1):
        for c in (-1, 0, 1):
            if r == 0 and c == 0: continue
            dir = (r, c)
            # directions.append(dir)
            count += check_direction(matrix, r, c, dir)
        
    return cound
        
        
if __name__ == '__main__':
    rows, cols = 5, 6
    matrix = generate_matrix(rows, cols)
    ## test printing
    print(matrix)
    print_matrix(matrix)
    print(line_column_size(matrix, 2, 1))

    r, c = 2, 3
    if is_in_limits(r, 0, rows) and is_in_limits(c, 0, cols):
        print("number is ok")

    r, c = 2, 6
    if is_in_limits(r, 0, rows) and is_in_limits(c, 0, cols):
        print("number is ok")
    else:
        print("number is not ok")

    # print(check_limits(5, 0, rows))
