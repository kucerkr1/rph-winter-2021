# RPH
Stránky předmětu:
[https://cw.fel.cvut.cz/b211/courses/b4b33rph/start](https://cw.fel.cvut.cz/b211/courses/b4b33rph/start)

## Cvičení (paralelka 108 v s211)

### CV01
- [Prezentace](https://docs.google.com/presentation/d/1DdLCvIraaLHPOlAaoPzSDnR0vbHd1WSv4bOC0MCwK2U/edit?usp=sharing)
- [kódy](cv01)


### CV02
- [Prezentace](https://docs.google.com/presentation/d/1J-D1BHB58t9EjJ54bkwvIGBAjaBFVKAb5x9B1JtwZ_M/edit?usp=sharing)
- [kódy](cv02)

### CV03
Konzultační cvičení, opakování větvení, cyklů, tříd.
- [kódy](cv03)

### CV02
Ohledně diskuze nad vyhodnocení turnaje vězňova dilema: Na stránkách [specifikace](https://cw.fel.cvut.cz/wiki/courses/b4b33rph/cviceni/veznovo_dilema/specifikace) je odkaz na [vyhodnocující kódy](https://gitlab.fel.cvut.cz/RPH-student-materials/pd)

- [Prezentace](https://docs.google.com/presentation/d/1w1iOe37yslbcDmPeyrrDOE2Ntc8DfpJk5O63BkrfGHk/edit?usp=sharing)
- [kódy](cv04)


### CV05
- [Prezentace](https://docs.google.com/presentation/d/1vmN-YyWgt-1d_Cew-RNOQS8JXPc64VooPXnnsiC2Ims/edit?usp=sharing)
- [kódy](cv05)


### CV06
Konzultační cvičení, opakování, generátory. **Připomenutí domácího úkolu"**: `print_matrix`, `generate_matrix`, `line_size`

- [kódy](cv06)


### CV07
- [Prezentace](https://docs.google.com/presentation/d/1FWAOIcAUkpzPHBcxAY5OAC0J2mGVWFxUy-IYNznAzag/edit?usp=sharing)
- [kódy](cv07)


### CV08
- [Prezentace](https://docs.google.com/presentation/d/1VMTZehhT8Cd35JhcZapJ36tQl8Ev3DJtZGy1ijLrC6I/edit?usp=sharing)
- [kódy](cv08)


### CV09
Náhradní cvičení, práce se soubory, rychlý souhrn v CV10.

### CV10
Soubor `utils.py` obsahuje funkci pro spam filtr, na konci je ukázka volání unit testů.
Unit testy lze pustit i z příkazové řádky: `python test_*`

Zbylé soubory slouží pro ukázku, co se stane při špatném kódování souboru.

- [Prezentace](https://docs.google.com/presentation/d/13Wabnl-cqiNFLQ4_Pz0nHe-NRvSbX_PlGmNpjK9t8ic/edit?usp=sharing)
- [kódy](cv10)

### CV11
Testování souborů, víceméně podle cvičení: [https://cw.fel.cvut.cz/.../b4b33rph/cviceni/program_po_tydnech/tyden_09](https://cw.fel.cvut.cz/wiki/courses/b4b33rph/cviceni/program_po_tydnech/tyden_09)

- [Prezentace](https://docs.google.com/presentation/d/1FaX1t_qUfy5pcVKAG1h8tYKl8YGmDUjYH-DJ-Um2BW4/edit?usp=sharing)
- [kódy](cv11)

### CV12
- [Prezentace](https://docs.google.com/presentation/d/1pvqv8DO5Pl6ZVjDdRZxjTexD86-P_w_QCen2-rKTpfs/edit?usp=sharing)
- [kódy](cv12)

Při práci v týmu můžete použít `git` nebo jiný verzovací systém - pro případ že si řešení rozbijete / zhoršíte je více než vhodné mít zálohu.

Také je možné použít doplňky pro sdílení kódu - např:
- [**Code With Me**](https://www.jetbrains.com/code-with-me/) v PyCharm, naleznete pod menu Tools
- [**LiveShare**](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare) do [Visual Studio Code](https://code.visualstudio.com/)




