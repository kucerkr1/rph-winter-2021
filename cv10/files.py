## Different file formatings read demonstration
## In your codes, UTF-8 should be used the most.

if __name__ == "__main__":
    ## Correct encoding
    f1 = open("t_utf.txt", encoding="utf-8")
    f2 = open("t_win.txt", encoding="windows-1250")

    print("UTF", f1.read())
    print("WIN", f2.read())

    f1.close()
    f2.close()

    ## WRONG encoding
    f1 = open("t_utf.txt", encoding="windows-1250")
    f2 = open("t_win.txt", encoding="utf-8")

    ## weird characters only
    print("UTF", f1.read())
    ## read error
    print("WIN", f2.read())
    
    f1.close()
    f2.close()
