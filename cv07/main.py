import time
import random

def test_set():
    '''Show usage of python built-in set'''

    ## demonstrate set operations
    arr = [1, 2, 2, 3]
    my_set1 = set(arr)
    my_set2 = set([2, 3, 4, 4])

    print("arr", arr)
    print("set", set(arr))
    print("set2", my_set2)

    set_union = my_set1 | my_set2
    print("union", set_union)

    set_intersect = my_set1 & my_set2
    print("intersect", set_intersect)

def arr_yield(N = 100):
    for i in range(N):
        yield i

def arr_list(N = 100):
    arr = [i for i in range(N)]
    return arr

def test_generators():
    """ demonstrate generator vs. array """
    iter = 10000000
    ## measure time with yield generator
    start_time1 = time.time()
    suma1 = 0
    for e in arr_yield(iter):
        suma1 += e
    print("Time yield:", time.time() - start_time1)

    ## measure time with returned list
    start_time2 = time.time()
    suma2 = 0
    for e in arr_list(iter):
        suma2 += e
    print("Time list :", time.time() - start_time2)

    assert suma1 == suma2


    ## generators with built-in sum
    iter2 = iter
    start_time1 = time.time()
    sum(arr_yield(iter2))
    print("sum yield", time.time() - start_time1)

    start_time2 = time.time()
    sum(arr_list(iter2))
    print("sum list ", time.time() - start_time2)

def test_dictionaries():
    '''dictionary usage'''
    my_dict1 = {}  ## create empty dictionary
    my_dict1["index1"] = 2
    my_dict1["toy"] = 10
    print(my_dict1)

    my_dict2 = {"index1": True, "toy": False}
    print(my_dict2)
    print(list(my_dict1.keys()))
    print(my_dict1.values())
    print(my_dict1.items())

    popped = my_dict1.pop("toy")
    print(popped)
    print(my_dict1)
    my_dict1.clear()
    print(my_dict1)

    dict_alph = {'a': 10, 'c': 1, 'b': 3}
    for key in dict_alph.keys():
        print(key, dict_alph[key], end='; ')
    print()  ## new line for readability
    ## print items in order sorted by key name
    for key in sorted(dict_alph.keys()):
        print(key, dict_alph[key], end='; ')
    print()

def test_list_comprehensions():
    ''' list comprehensions
        = generate array on one line using *for* keyword
    '''
    arr1 = [i for i in range(10)]
    arr1_eqi = list(range(10))  ## equivalent to above
    print(arr1)
    print(arr1_eqi)

    arr2 = [i ** 2 for i in range(10)]
    print(arr2)
    arr_rand1 = [random.random() for _ in range(5)]
    arr_rand2 = [random.randint(1, 10) for _ in range(5)]
    for _ in range(6):  ## _ can be used in for also
        pass
    print(arr_rand1)
    print(arr_rand2)

if __name__ == '__main__':
    ## Uncomment which section you want to execute

    # test_set()
    # test_generators()
    # test_dictionaries()
    # test_list_comprehensions()

    pass ## default for indentation if all is commented out


