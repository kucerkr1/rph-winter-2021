import time

# N = 10000000
N = 100
def generator_yield():
    for i in range(N):
        yield i

def generator_array(num:int = N) -> list:
    ## some comment that is not visible in help(generator_array)
    '''
    Generate using list comprehension

    :param num (integer): length of the list
    '''
    return [i for i in range(num)]

# Comment before function
def bad_formating_example(): # comment at same line
    """
    Function that displays wrong formating
    this is penalized in manual control.
    """
    a = [i for i in range(5)];arr = []

## parse
    for i in range(N):
        arr.append( i )
    return a
    arr = [1,2, 3,  4 , 0]
    a = 2+ 1 *(  (5 -6) / 1)


if __name__ == '__main__':
    start_time = time.time()
    suma = 0
    for elem in generator_array(N):
        # print(elem, end = " ")
        suma += elem
    print("\narray",time.time() - start_time)

    start_time = time.time()
    suma = 0
    for elem in generator_yield():
        # print(elem, end = " ")
        suma += elem
    print("\nyield", time.time() - start_time)
