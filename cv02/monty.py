# Simulation of Monty Python problem
import random

def monty(strategy):
    """
    Simulate this sequence of events:
        1. The host randomly chooses a 'car' door
        2. The contestant randomly makes a 'pick'
        3. The host randomly selects a non-car, non-pick door to be 'opened.'
        4. If strategy == 'switch', contestant changes 'pick'            to the other unopened door
        5. Return true if the pick is the door with the car.
    """
    if type(strategy) != str:
       return "Not a valid strategy"

    doors = [1, 2, 3]
    car = random.choice(doors)
    pick = random.choice(doors)
    opened = 0
    for door in doors:
        if door != car and door != pick:
            opened = door
            break
    if strategy == "switch":
        for door in doors:
            if door != pick and door != opened:
                pick = door
                break
    # else:
    #     pick = pick
    if pick == car:
        return True
    else:
        return False


if __name__ == '__main__':
    print(monty(1))
    iter = 10000
    win = 0
    for _ in range(iter):
        result = monty("switch")
        if result:
            win += 1
    print("Wins", win, "out of", iter)
    print("Percentage wins", (win / iter) * 100, "%")
