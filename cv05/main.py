class BasePlayer:
    def __init__(self, player):
        self.player = player

    def move(self):
        pass

    def say_hi(self):
        print("Hi all")

class ExtraPlayer:
    def __init__(self, player):
        self.player = player

    def say_bye(self):
        print("Bye")

    def say_hi(self):
        print("Not Bye")

class MyPlayer(ExtraPlayer, BasePlayer):
    def __init__(self, player, num = 0):
        super().__init__(player)
        self.num = num

    def move(self):
        return True

COOP = False
DEF = True
ME = 0
OTHER = 1
class PDPlayer:
    payoff_matrix = {} 
    op_last_move = None
    my_moves = []
    op_moves = []

    def __init__(self, payoff_matrix, num_games = 0):
        self.payoff_matrix[COOP, COOP] = payoff_matrix[0][0]
        self.payoff_matrix[COOP, DEF] = payoff_matrix[0][1]

        self.num_games = num_games
        self.strategy = None ## very logical thing

      

    def move(self):
        a = self.payoff_matrix[COOP, COOP][ME]
        if a > 3:
            return COOP
        elif a < 2:
            return DEF 
        elif self.op_last_move == COOP:
            return DEF
               
        return DEF

    def my_last_move(self):
        return self.my_moves[-1]

    def record_moves(self, mine, other):
        self.my_moves.append(mine)
        self.op_moves.append(other)

        self.op_last_move = other
        ## DUPLICATE VALUES
        # self.my_last_move = mine
        # self.op_last_move = other


def dict_test():
    my_dict = {100: 5, 200: 3}
    my_dict[200.21] = 10
    my_dict[0.1] = 0
    my_dict['m'] = 0

    print(my_dict)
    print(my_dict.keys())
    print(my_dict.values())

    for key, value in my_dict.items():
        print(key, my_dict[key])

    my_dict.pop('m')
    print(my_dict.items())

if __name__ == "__main__":
    # dict_test()
    name = "BasePlayer"
    player = BasePlayer(name) 
    my_player = MyPlayer("MyPlayer", 10) 
    print(player.player)
    print(my_player.player)
    player.say_hi()
    my_player.say_hi()
    my_player.say_bye()
    print(player.move())
    print(my_player.move())

    payoff_matrix = (((4,5), (1,60)), ((60,1), (5,4)))
    my_pd = PDPlayer(payoff_matrix, 0)
    print(my_pd.move())