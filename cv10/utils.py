
## utils.py for spam filter HW

def read_classification_from_file(filename):
    ## another way to load a file
    # f = open(filename, 'r', encoding='utf-8')
    # lines = f.readlines()
    # print(lines)
    # f.close()

    classif = {}
    with open(filename, 'r', encoding='utf-8') as f:
        for line in f:
            ret = line.split("\n")[0]
            key, val = ret.split(" ")
            ## also ok, but can be unpacked on one line
            # ret = ret.split(" ")
            # key, val = ret[0], ret[1]
            print(key, val)
            classif[key] = val
    # print(classif)

    return classif



if __name__ == '__main__':
    filename = "1/!truth.txt"
    read_classification_from_file(filename)
    

    ## Run all the tests from file
    import unittest
    from test_readClassificationFromFile import *
    unittest.main()