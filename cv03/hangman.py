## Constant with hangman ascii art
HANGMANPICS = ['''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']

class HangMan:
    gameon = True

    def __init__(self, word):
        self.word = word
        self.number_of_tries = len(HANGMANPICS) - 1
        ## other params
        self.letters = ['_' for i in range(len(word))]

        print(HANGMANPICS[0])
        print("".join(self.letters))

    def check_if_guessed(self):
        ## check all letters
        for w in self.letters:
            if w == '_':    ## not guessed the whole word
                return "".join(self.letters)
        ## guessed all
        self.gameon = False
        return "YOU WIN, CONGRATS"


    def guess(self, letter):
        if not self.gameon:
            return "YOU LOST EARLIER"

        ## Is the letter in the word?
        found = False
        for i, w in enumerate(self.word):
            if w == letter:     ## Guess was correct
                self.letters[i] = letter    ## fill in "_"
                found = True    ## do not substract try

        ## letter is in the word, return updated letters
        if found:
            return self.check_if_guessed()

        ## letter not in word
        self.number_of_tries -= 1
        ## if no tries left, end the game
        if self.number_of_tries <= 0:
            self.gameon = False
            return "YOU LOSE" + HANGMANPICS[-1]   ## jump out of the function
        ## tries is not zero, return number of tries left
        return "Wrong letter, you have " + str(self.number_of_tries) \
               + " tries left.\n" + HANGMANPICS[len(HANGMANPICS) - self.number_of_tries - 1]



if __name__ == "__main__":
    word = "camel"
    ## create game
    game = HangMan(word)

    ## tries not zero and word not guessed
    while game.gameon:
        letter = input("Guess a letter:")
        print(game.guess(letter))