import random

def print_matrix(matrix):
    for r in range(len(matrix)):
        for c in range(len(matrix[r])):
            print(matrix[r][c], end=" ")
        print("\n", end="")


if __name__ == '__main__':
    rows, cols = 5, 6
    matrix = [[random.randint(0, 1) 
                for _ in range(cols)] 
                    for _ in range(rows)]

    matrix = [] 
    for r in range(rows):
        arr = [0]*cols
        for c in range(cols):
            arr[c] = random.randint(0, 1)
        matrix.append(arr)

    print(matrix)
    print_matrix(matrix)