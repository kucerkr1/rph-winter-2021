class Drink:
    def __init__(self, condiments=None):
        self.condiments = condiments
        self.put = None

    def boil_water(self):
        print("Vařím vodu.")

    def pour_water(self):
        print("Nalévám vodu do šálku.")

    def put_in_cup_arg(self, what):
        print("Dávám do šálku ", what, ".", sep="")

    def put_in_cup(self):
        raise NotImplementedError("Drink is not specific")

    # def add_condiments_arg(self, what):
    #     print("Přidávám ", what, ".", sep="")

    def add_condiments(self):
        if not self.condiments:
            raise NotImplementedError("Drink is not specific")
        print("Přidávám ", self.condiments, ".", sep="")

    def prepare(self):
        self.boil_water()
        self.put_in_cup()
        self.pour_water()
        self.add_condiments()

class Tee(Drink):
    def __init__(self, condiments=None, put="sáček čaje"):
        super().__init__(condiments)
        self.put == put

    def put_in_cup(self):
        # super.put_in_cup("sáček čaje")
        super().put_in_cup()
        print("Dávám do šálku sáček čaje.")

    # def add_condiments(self):
    #     print("Přidávám ", self.condiments, ".", sep="")

class Coffee(Drink):
    def put_in_cup(self):
        self.put_in_cup_arg("lžičku mleté kávy")

    # def add_condiments(self):
    #     self.add_condiments_arg(self.condiments)

if __name__ == "__main__":
    objednavka = [Coffee('mléko a cukr'),
                  Tee('citrón'),
                  Coffee('mléko')]
    for napoj in objednavka:
        napoj.prepare()
        print()