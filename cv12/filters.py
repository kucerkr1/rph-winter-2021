import pathlib
import random

TRUTH_FILE = "!truth.txt"
CLASSIF_FILE = "!prediction.txt"
POS_TAG = "SPAM"
NEG_TAG = "OK"

class BaseFilter:
    def decide(self, filename):
        raise NotImplementedError("Filter not specific")

    def train(self, dirname):
        """
        Learn to **classify emails** based on
        the contents of directory and file "!truth.txt"

        :param dirname: folder containing file "!truth.txt"
        :return: None
        :effects: setup filter
        """
        ## for reading truth file
        truth_path = pathlib.Path(dirname).joinpath(TRUTH_FILE)
        print(truth_path)

        ## read all emails
        path = pathlib.Path(dirname)
        print(path)
        files = [f for f in path.iterdir() if f.is_file()]

        classif_dict = {}
        for file in files:
            print(file)
            if "!" in file.name:
                continue
            ## make decision for file
            print(file.name, self.decide(file))
            classif_dict[file.name] = self.decide(file)

    def test(self, dirname):
        """
        Classify emails based on filter training
        the contents of directory.

        :param dirname: folder containing file "!truth.txt"
        :return: None
        :effects: setup filter
        """
        path = pathlib.Path(dirname)

        classif_dict = {}
        ## analyze files

        ## write classifion to file "!prediction.txt"
        # ...

class NaiveFilter(BaseFilter):
    def decide(self, filename):
        """Classifies all as ham"""
        return NEG_TAG


class ParanoidFilter(BaseFilter):
    def decide(self, filename):
        """Classifies all as spam"""
        return POS_TAG


class RandomFilter(BaseFilter):
    def decide(self, filename):
        """Classifies randomly"""
        return random.choice([NEG_TAG, POS_TAG])

if __name__ == "__main__":
    filter = NaiveFilter()
    filter.train("1")