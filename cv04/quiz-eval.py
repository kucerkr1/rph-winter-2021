## Evaluate 2/3 of avg team quiz question
import matplotlib.pyplot as plt

def get_avg_closest(arr):
    '''
    Compute winner value from arr, that is closest
    to 2/3 of arr average
    :param arr:
    :return: avg, 2/3 avg, winner
    '''
    avg = sum(arr) / len(arr)
    val = avg * (2 / 3)
    ## init values
    min_diff = max(arr)
    best_a = arr[0]

    for a in arr:
        diff = abs(a - val)
        if diff < min_diff:
            min_diff = diff
            best_a = a

    return avg, val, best_a

if __name__ == '__main__':
    arr = [66, 2, 20, 6, 4, 27, 6, 4, 86, 50, 21, 15]
    avg, val, a = get_avg_closest(arr)
    print("Average:", avg)
    print("2/3 avg:", val)
    print("Winner :", a)

    plt.yticks([1, 2])
    plt.xticks(range(0, 101, 10))
    plt.hist(arr, range(0, 100))
    plt.show()
