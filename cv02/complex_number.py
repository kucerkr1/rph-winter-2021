## Complex Number Class
## author: kucerkr1
## date: 01.10.2021
from math import sqrt

## class == CZ třída
## object, instance == CZ instance

class ComplexNumber:
    re = 0
    im = 0

    def __init__(self, re, im):
        self.re = re
        self.im = im

    def __str__(self):
        return str(self.re) + '+' + str(self.im) + 'i'
    ##Do this
    def size(self):
        return sqrt(self.re**2 + self.im**2)

    def __abs__(self):
        return sqrt(self.re**2 + self.im**2)

    def inc_parts(self, a_re, a_im):
        self.re += a_re
        self.im = self.im + a_im

    def inc(self, other):
        self.re += other.re
        self.im += other.im

    def add(self, other):
        return ComplexNumber(self.re + other.re,
                             self.im + other.im)

    def __add__(self, other):
        return ComplexNumber(self.re + other.re,
                             self.im + other.im)

    def __mul__(self, other):
        return ComplexNumber(self.re * other.re,
                             self.im * other.im)

    def __sub__(self, other):
        return ComplexNumber(self.re - other.re,
                             self.im - other.im)



if __name__ == "__main__":
    a = ComplexNumber(3, 4)

    print(f"{a.re=}")
    print(f"{a.im=}")
    print("a", a)
    print("a abs old", a.size())
    print("a abs new", abs(a))

    a.inc_parts(2, 2)
    print("a inc", a)

    b = ComplexNumber(3, 3)
    a.inc(b)
    print("a inc b", a)

    c = a.add(b)
    print("c", c)
    print("a", a)

    print("Add", a + b)
    print("Mul", a * b)
    print("Sub", a - b)